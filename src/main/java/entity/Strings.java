package entity;

import java.util.Arrays;
import java.util.Objects;

public class Strings {
    public String string1;
    public String string2;
    public String word;
    public String punctuationMark;
    public String[] subStr1;
    public String[] subStr2;

    public Strings() {
    }

    public Strings(String string1, String string2) {
        this.string1 = string1;
        this.string2 = string2;
    }

    public Strings(String string1, String word, String punctuationMark) {
        this.string1 = string1;
        this.word = word;
        this.punctuationMark = punctuationMark;
    }


    public String getString1() {
        return string1;
    }

    public void setString1(String string1) {
        this.string1 = string1;
    }

    public String getString2() {
        return string2;
    }

    public void setString2(String string2) {
        this.string2 = string2;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPunctuationMark() {
        return punctuationMark;
    }

    public void setPunctuationMark(String punctuationMark) {
        this.punctuationMark = punctuationMark;
    }

    public String[] getSubStr1() {
        return subStr1;
    }

    public void setSubStr1(String[] subStr1) {
        this.subStr1 = subStr1;
    }

    public String[] getSubStr2() {
        return subStr2;
    }

    public void setSubStr2(String[] subStr2) {
        this.subStr2 = subStr2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Strings strings = (Strings) o;
        return Objects.equals(string1, strings.string1) &&
                Objects.equals(string2, strings.string2) &&
                Arrays.equals(subStr1, strings.subStr1) &&
                Arrays.equals(subStr2, strings.subStr2);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(string1, string2);
        result = 31 * result + Arrays.hashCode(subStr1);
        result = 31 * result + Arrays.hashCode(subStr2);
        return result;
    }

    @Override
    public String toString() {
        return "Strings{" +
                "string1='" + string1 + '\'' +
                ", string2='" + string2 + '\'' +
                ", subStr1=" + Arrays.toString(subStr1) +
                ", subStr2=" + Arrays.toString(subStr2) +
                '}';
    }
}